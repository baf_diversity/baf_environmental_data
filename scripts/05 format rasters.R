#tudo em asc tudo sem NAs
require(rgeos)
library(raster)
library(rgdal)
library(dplyr)
library(textclean)
library(readxl)
#--------------------------#
# Raster Mata Atlântica ####
#--------------------------#
rm(list = ls())
#selects resolution
resol <- 0.5 #0.1666, 0.08333
name <- case_when(resol == 0.5 ~ "50km",
                  resol == 0.1666 ~ "20km",
                  resol == 0.08333 ~ "10km")
surveys <- "surveys"
surveys2 <- paste0(surveys, "_noSC")

inv  <- list.files(paste0("./Dados/Raster/", name, "/",surveys), full.names = T, pattern = ".tif$")
inv2  <- list.files(paste0("./Dados/Raster/", name, "/",surveys2), full.names = T, pattern = ".tif$")
occ  <- list.files(paste0("./Dados/Raster/", name), full.names = T, pattern = "tif$")
inv_r  <- stack(inv)
inv_r2  <- stack(inv2)
occ_r  <- stack(occ)

####stacks

all <- stack(occ_r, inv_r, inv_r2)
names(all)
writeRaster(all,
            filename = paste0("./Dados/Raster/",name,"/",name),
            bylayer = T,
            suffix = "names",
            format = "ascii")
